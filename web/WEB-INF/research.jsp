<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Tor Metrics Portal: Research</title>
  <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
  <link href="/css/stylesheet-ltr.css" type="text/css" rel="stylesheet">
  <link href="/images/favicon.ico" type="image/x-icon" rel="shortcut icon">
</head>
<body>
  <div class="center">
    <%@ include file="banner.jsp"%>
    <div class="main-column">
        <h2>Tor Metrics Portal: Research</h2>
        <br>
        <p>The Tor Metrics Project aims at supporting privacy enhancing
        technologies research by making gathered network
        <a href="data.html">data</a>, and
        <a href="tools.html">tools</a>
        for processing these data available to the public.
        Some results from analyzing these data can be found in
        <a href="https://research.torproject.org/techreports.html">Tor
        Tech Reports</a>.  If you are
        missing anything for your Tor-related research or want to share
        your research results with others, please
        <a href="mailto:tor-assistants@torproject.org">let us know</a>!
    </div>
  </div>
  <div class="bottom" id="bottom">
    <%@ include file="footer.jsp"%>
  </div>
</body>
</html>
